<?php

namespace AppBundle\Controller;

use pimax\FbBotApp;
use pimax\Messages\ImageMessage;
use pimax\Messages\Message;
use pimax\Messages\MessageButton;
use pimax\Messages\MessageElement;
use pimax\Messages\StructuredMessage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DefaultController extends Controller
{
    const FB_URI = 'https://graph.facebook.com/v2.6/me/messages';
    const PAGE_ACCESS_TOKEN = 'EAAUP95T7qO8BAJTl59tlPM37Rk78Iq1zyORyQs8ii2uVCfOmLZCxKqq4Rm0itwNrcfyqF09ZAUQ885iSVX9wn2tsTGGbqJyZB0Fk6TIZBKJ2YUzrZAvRgX2jQBqUnMuPbJYMchDsX9JbQkIZC1fCyq3DIHVYOh8zoHBwOv8Hg8IQZDZD';
    const TYPE_POST = 'post';
    const TYPE_GET = 'get';

    private $_bot;
    private $_senderId;
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }
     
     /**
     * @Route("/webhook", name="facebookhook")
     * @Method({"GET"})
     */
    public function webhookVerifyAction(Request $request)
    {
        $token = 'i_am_the_god_of_hell_fire';
        if($_GET['hub_mode'] === 'subscribe' && $_GET['hub_verify_token'] === $token){
            return new Response($_GET['hub_challenge']);
        }

        throw new AccessDeniedHttpException();
    }

    /**
     * @Route("/webhook", name="facebookhook-post")
     * @Method({"POST"})
     */
    public function webhookAction(Request $request)
    {
        error_reporting(E_ALL);

        error_log('Hello nurse');
        $json = file_get_contents('php://input');
$data = json_decode( $json, true );

error_log(print_r($data,1));
        if($data['object'] == 'page'){
            $this->_bot = new FbBotApp( self::PAGE_ACCESS_TOKEN );
error_log(print_r($this->_bot,1));
            foreach($data['entry'] as $entry){
                $pageId = $entry['id'];
                $time = $entry['time'];

                foreach ($entry['messaging'] as $messagingEvent) {
                    $this->_senderId = $messagingEvent['sender']['id'];

                    // Skipping delivery messages
                    if (!empty($messagingEvent['delivery'])) {
                        continue;
                    }

                    // When bot receive message from user
                    if (!empty($messagingEvent['message'])) {
                        $command = $messagingEvent['message']['text'];
                        // When bot receive button click from user
                    } else if (!empty($messagingEvent['postback'])) {
                        $command = $messagingEvent['postback']['payload'];
                    }


                    if(stripos($command, 'hello') !== false){
                        $this->_bot->send(new Message($this->_senderId, 'Hello! How can I serve you master?'));
                        return new Response('');
                    }

                    if(stripos($command, 'lucky') !== false){
                        $message = new StructuredMessage(
                            $this->_senderId,
                            StructuredMessage::TYPE_GENERIC,
                            [
                                'elements' => [
                                    new MessageElement(
                                        'Best product evah',
                                        'Buy it now do not be stupid',
                                        'http://assets1.actievandedag.nl/wp-content/uploads/2016/07/Strapless-bra.-503x295.jpg',
                                        array(
                                            new MessageButton(MessageButton::TYPE_WEB, 'Buy now!', 'http://www.actievandedag.nl/actie-aanbieding/08-07-2016-we-can-make-it-bra-bh')

                                        )
                                    )
                                ]
                            ]
                        );

                        $this->_bot->send($message);
                        return new Response('');
                    }

                    $words = [
                        "hotels" => [
                            "hotels", "hotel", "sleep",
                        ],
                        "restaurants" => [
                            "restaurants", "restaurant", "hungry", "eat", "steak",
                        ],
                        "producten" => [
                            "stuff", "buy", "gift", "xmas", "products","product"
                        ],
                        "avondje-uit" => [
                            "go out", "day out", "surprise", "girlfriend", "boyfriend"
                        ],
                        "vakanties" => [
                            "vacation", "city break", "tired"
                        ],
                    ];

                    $detectedCategory = null;
                    foreach ($words as $category => $keywords) {
                        foreach ($keywords as $keyword) {
                            if (stripos($command, $keyword) !== false) {
                                $detectedCategory = $category;
                                break 2;
                            }
                        }
                    }

                    if (empty($detectedCategory)) {
                        $this->offerCategoryChoice();
                    } else {
                        $this->getCategory($detectedCategory);
                    }
                }
            }
        }


        return new Response('ok');
    }

    private function offerCategoryChoice()
    {
        $categories = [
            "hotels" => "Hotels",
            "restaurants" => "Restaurants",
            "producten" => "Products",
        ];

        $buttons = [];
        foreach ($categories as $id => $name) {
            $buttons[] = new MessageButton(
                MessageButton::TYPE_POSTBACK,
                $name,
                $id
            );
        }
        $message = new StructuredMessage(
            $this->_senderId,
            StructuredMessage::TYPE_GENERIC,
            [
                'elements' => [
                    new MessageElement(
                        "What you say esse?! Maybe you want something from this list?",
                        "No comprendo.",
                        "",
                        $buttons
                    )
                ]
            ]
        );

        $this->_bot->send($message);
    }

    protected function getCategory($category)
    {
        $greetings = array(
            'Sure thing, we have %d %s! Here are the top 3:',
            'Great idea! We currently have %d %s! Here are the top 3:',
            'You are on fire! We currently have %d %s! Here are the top 3:',
            'We might have something for that! We currently have %d %s! Here are the top 3:',
            'You came to the right place! We currently have %d %s! Here are the top 3:',
            'You got it! We have %d %s! Here are the top 3:',
        );

        $deals = array();
        $dealsObjs = array();

        $result = $this->call($category);
        $count = $result['hits'];
        $deals = $result['deals'];

        if(!empty($deals)){
            $key = array_rand($greetings);
            $greeting = sprintf($greetings[$key],$count, $category);
            $this->_bot->send(new Message($this->_senderId, $greeting));

            foreach($deals as $deal){

                $deal['thumbnailMediumUrl'] = 'http:'.$deal['thumbnailMediumUrl'];

                $dealsObjs[] = new MessageElement(
                    $deal['title'],
                    $deal['offeringShortDescription'],
                    $deal['thumbnailMediumUrl'],
                    array(
                        new MessageButton(MessageButton::TYPE_WEB, 'See details', 'http://www.actievandedag.nl/actie-aanbieding/'.$deal['permalink']),
                        new MessageButton(MessageButton::TYPE_WEB, 'Buy now!', $deal['bookingUrl']),
                        new MessageButton(MessageButton::TYPE_WEB, 'See all '.$category,'http://www.actievandedag.nl/categorie/'.$category)
                    )
                );
            }

            $message = new StructuredMessage(
                $this->_senderId,
                StructuredMessage::TYPE_GENERIC,
                [
                    'elements' => $dealsObjs
                ]
            );

            $this->_bot->send($message);
        }
    }

    /**
     * Request to API
     *
     * @param $data Data
     * @param string $type Type of request (GET|POST)
     * @return array
     */
    protected function call($category = 'hotels')
    {
        $headers = [
            'Content-Type: application/json',
        ];

        $process = curl_init( 'http://actievandedag.nl/feed/'.$category.'/3/0' );

        curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($process, CURLOPT_HEADER, false);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);

        curl_setopt($process, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($process);
        curl_close($process);

//        $json = '{"hits":36,"count":3,"deals":[{"id":"253731","thumbnail":"\u003Cimg class=\u0022responsive-image pure-img\u0022 data-src=\u0022http:\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Verblijf-in-je-eigen-hotelhuisje.-1-421x265.jpg\u0022 src=\u0022http:\/\/www.actievandedag.nl\/wp-content\/themes\/actievandedag.nl-v4\/library\/images\/placeholder.png\u0022 \/\u003E","thumbnailUrl":"\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Verblijf-in-je-eigen-hotelhuisje.-1.jpg","thumbnailMediumUrl":"\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Verblijf-in-je-eigen-hotelhuisje.-1-503x370.jpg","bookingUrl":"https:\/\/www.actievandedag.nl\/boeken\/prijsberekening\/253731","startTime":1466546400,"endTime":1468360800,"title":"3 dagen je eigen hotelhuisje","category":"","categoryType":"1","affiliateCategoryType":6,"categories":["Er op uit","Hotels","Hotelsoverig","Kortingscode","Nieuw op ActievandeDag","Romantiek"],"categoriesSlugs":["er-op-uit","hotels","hotelsoverig","kortingscode","nieuw","romantiek"],"images":["\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Verblijf-in-je-eigen-hotelhuisje-4.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Kamer-Restinn-Rijsbergen-1.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Huisje-Restinn-Rijsbergen-1.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Restinn-Rijsbergen.jpg","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Huisjes-Restinn-Rijsbergen.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Hotelhuisje-1.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Uitzicht-Restin-Rijsbergen-1.jpg","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Hotelhuisje-Restinn-Rijsbergen.jpg"],"imagesMedium":["\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Verblijf-in-je-eigen-hotelhuisje-4-503x370.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Kamer-Restinn-Rijsbergen-1-503x370.jpg","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Huisje-Restinn-Rijsbergen-1-503x370.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Restinn-Rijsbergen-503x370.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Huisjes-Restinn-Rijsbergen-503x370.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Hotelhuisje-1-503x370.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Uitzicht-Restin-Rijsbergen-1-503x370.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Hotelhuisje-Restinn-Rijsbergen-503x370.jpg"],"permalink":"22-06-2016-restinn-rijsbergen","cornerText":"41% korting","rating":3,"offeringList":["Luxe in Restinn Rijsbergen","\nEigen hotelhuisje + ontbijt","\nZoover-beoordeling: 8.9"],"dealOrders":126,"position":3,"status":0,"author":"737124","adminTitle":"22-06-2016 Restinn Rijsbergen","accomodationTitle":"Restinn Rijsbergen","originalPricePrefix":"","originalPrice":220,"startingPricePrefix":"v.a.","startingPrice":129,"startingPricePostfix":"p. verblijf","savePercentage":41,"dealConfigurationId":8698,"invoiceInfo":"U heeft een 3-daags verblijf bij Restinn Rijsbergen geboekt. Dit is inclusief 2 overnachtingen in je eigen Restinn hotelhuisje, 2x uitgebreid ontbijt, gratis parkeren en gratis WiFi.","extraInvoiceInfo":"U heeft de betaling voldaan aan ActievandeDag. De toeristenbelasting dient u ter plaatse te voldoen. Het hotel stuurt u geen bevestiging meer. U kunt deze bevestiging printen en meenemen naar het hotel om in te checken. Inchecken is mogelijk vanaf 14:30 uur tot 19:00 uur.\n\nWijzigen of annuleren is niet mogelijk via ActievandeDag. Indien u vragen heeft over uw boeking kunt u contact opnemen met het hotel via telefoonnummer 020 - 460 91 50.\n\nTer plaatse te betalen:\nToeristenbelasting: \u20ac 1,09 p.p.p.n.\n\nAdres:\nRestinn Rijsbergen\nOekelsestraat 2A\n4891 PG Rijsbergen\nNederland\nTelefoon: 020 - 460 91 50","bccEmailAddress":"","ticketDescription":"","ticketInstructions":"","ticketImage":"","ticketColorAccent":"","partnerId":839827,"partnerName":"Restinn Rijsbergen","siteLabel":1308,"country":"","city":"Rijsbergen","latitude":51.508003,"longitude":4.713681,"accomodationProperties":[""],"carePackage":[""],"offeringShortDescription":"Geniet van een ontspannen verblijf bij Restinn Rijsbergen. Geniet van de omgeving of ga een dagje naar het nabij gelegen Breda of het Vlaamse Antwerpen.","tab1Label":"","tab2Label":"","tab3Label":"","tab1Content":"\u003Ch3\u003ELuxe 3-daags verblijf in je eigen hotelhuisje\u003C\/h3\u003E\n\u003Cul\u003E\n\t\u003Cli\u003EGeniet van een luxe verblijf in\u00a0Restinn Rijsbergen\u003C\/li\u003E\n\t\u003Cli\u003E2 nachten in je eigen hotelhuisje inclusief ontbijt\u003C\/li\u003E\n\t\u003Cli\u003EOntdek de wandel- en fietsroutes in de omgeving\u003C\/li\u003E\n\t\u003Cli\u003EGa een dagje winkelen in Breda of Antwerpen\u003C\/li\u003E\n\t\u003Cli\u003EHet hotel is op Zoover beoordeeld met een 8.9\u003C\/li\u003E\n\u003C\/ul\u003E\n\u003Ch3\u003EInclusief\u003C\/h3\u003E\n\u003Col\u003E\n\t\u003Cli\u003E2 overnachtingen in je eigen Restinn hotelhuisje\u003C\/li\u003E\n\t\u003Cli\u003E2x uitgebreid ontbijt\u003C\/li\u003E\n\t\u003Cli\u003EGratis parkeren\u003C\/li\u003E\n\t\u003Cli\u003EGratis WiFi\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EExclusief\u003C\/h3\u003E\n\u003Col\u003E\n\t\u003Cli\u003EToeristenbelasting: \u20ac 1,09 p.p.p.n.\n(verplicht en ter plaatse te voldoen)\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003ERestinn Rijsbergen\u003C\/h3\u003E\nIn het prachtige landgoed \u0022De Baronie\u0022 kun je bij Restinn Rijsbergen heerlijk overnachten in de luxe hotelhuisjes. De omgeving cre\u00ebert ontmoetingen met platteland, weelderige grasvelden, kleurrijke veldbloemen, bruisende steden en kleine dorpjes met een bries van het verleden.\n\nRestinn Rijsbergen wordt ge\u00ebxploiteerd door Wim en Joke Jochems. Van oorsprong runde zij een gemengde boerderij, maar hebben zich nu gericht op de recreatie. Zij zullen er voor zorgen dat je heerlijk kunt genieten en vertellen je graag meer over Rijsbergen en haar bijzondere plekjes!\n\nRestinn staat voor bijzonder overnachten en buitengewoon genieten, het hele jaar door. Een Restinn is een vrijstaand hotelhuisje. Bij Restinn kun je onthaasten, bijkomen en bovendien de natuur en het boerenleven optimaal in u opnemen. Aan alles is gedacht om u van een heerlijk verblijf te verzekeren.\n\u003Ch3\u003EJouw Restinn hotelhuisje\u003C\/h3\u003E\nDe hotelhuisjes (een exclusief ontwerp van architect Henri Leloup) hebben een volledig doorzicht, waardoor buiten- en binnenruimte speels door elkaar lopen. Ieder huisje heeft openslaande deuren naar het terras en is zeer compleet ingericht met onder meer:\n\u003Col\u003E\n\t\u003Cli\u003Etwee eenpersoons bedden (boxspring, incl. bedlinnen)\u003C\/li\u003E\n\t\u003Cli\u003Edouche en toilet (badlinnen aanwezig)\u003C\/li\u003E\n\t\u003Cli\u003Ecomfortabel zitje\u003C\/li\u003E\n\t\u003Cli\u003Egezellige eethoek\u003C\/li\u003E\n\t\u003Cli\u003Ekoffie- en thee voorzieningen\u003C\/li\u003E\n\t\u003Cli\u003Etelevisie met ruim kanalenaanbod\u003C\/li\u003E\n\t\u003Cli\u003Egratis internetaansluiting (eigen laptop meenemen)\u003C\/li\u003E\n\t\u003Cli\u003Eeigen terras met zitje en vrij uitzicht\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003ESmullen en bijkomen\u003C\/h3\u003E\nBij iedere Restinn overnachting hoort een lekker ontbijt, verzorgd \u00e9n bezorgd door de gastvrouw of gastheer. Aarzel niet ter plekke te vragen naar de mogelijkheden te genieten van een uitgebreide lunch of overheerlijk diner, naar wens met een passende wijn!","tab2Content":"\u003Ch3\u003EOmgeving\u003C\/h3\u003E\nDe locatie Rijsbergen, die gelegen is in het BuitenGoed, is d\u00e9 uitvalsbasis om diverse fiets en wandelroutes te maken. Tevens kun je de bruisende stad Breda en de Vlaamse stad Antwerpen ontdekken, maar ook dichterbij komen bij de oorsprong van de beroemdste Nederlandse schilder Vincent van Gogh.\n\nTijdens je verblijf nabij Breda hoef jij je zeker niet te vervelen! Breda biedt vele bezienswaardigheden, zoals het Breda\u2019s Museum, het Graphic Design Museum, de Grote Kerk. Ook voor de sportiveling heeft Breda genoeg mogelijkheden, met mooie wandel \u2013en fietsroutes in de directe omgeving en een tochtje in een moterbootje of kano over de singels is zeker een aanrader.\n\u003Ch3\u003ELocatie\u003C\/h3\u003E\n\u003Cp class=\u0022MsoNormal\u0022\u003E[sgmap addr=\u0022Breedschotsestraat 3 Rijsbergen\u0022]\u003C\/p\u003E\n\u003Cp class=\u0022MsoNormal\u0022\u003E\u003C\/p\u003E\n\u003Cp class=\u0022MsoNormal\u0022\u003E\u003C\/p\u003E","tab3Content":"\u003Ch3\u003EVoorwaarden actie\u003C\/h3\u003E\n\u003Col\u003E\n\t\u003Cli\u003EDeze actie is alleen te boeken voor de aangegeven data.\u003C\/li\u003E\n\t\u003Cli\u003EDeze actie is alleen te boeken \u003Cstrong\u003Evan woensdag 22 juni\u00a000:00 uur t\/m dinsdag 12 juli\u00a023:59 uur.\u003C\/strong\u003E\u003C\/li\u003E\n\t\u003Cli\u003EMaximaal 2 personen per boeking, geen uitzonderingen mogelijk\u003C\/li\u003E\n\t\u003Cli\u003EIndien je voor meerdere personen wilt boeken, dien je meerdere boekingen te maken.\u003C\/li\u003E\n\t\u003Cli\u003ELet op: per datum is slechts een beperkt aantal kamers beschikbaar. Indien je meerdere boekingen voor 1 datum wilt maken, kun je voor informatie over de beschikbaarheid per datum contact opnemen met onze klantenservice.\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EKortingen en toeslagen\u003C\/h3\u003E\n\u003Col\u003E\n\t\u003Cli\u003EOp deze actie is geen kinderkorting van toepassing.\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EVoorwaarden hotel\u003C\/h3\u003E\n\u003Col\u003E\n\t\u003Cli\u003EInchecken vaf 14:30 -\u00a019:00 uur en uitchecken tot 11:00 uur.\u003C\/li\u003E\n\t\u003Cli\u003EJe kunt gratis parkeren bij het hotel.\u003C\/li\u003E\n\t\u003Cli\u003EHuisdieren zijn niet toegestaan.\u003C\/li\u003E\n\t\u003Cli\u003EHet hotel is niet geschikt voor mindervaliden.\u003C\/li\u003E\n\t\u003Cli\u003EMinimale leeftijd hoofdboeker: 18 jaar.\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EWijzigen en annuleren\u003C\/h3\u003E\n\u003Col\u003E\n\t\u003Cli\u003EWijzigen of annuleren is niet mogelijk via ActievandeDag.\u003C\/li\u003E\n\t\u003Cli\u003EIn geval van annulering of no-show is restitutie niet mogelijk.\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EBoekingsproces\u003C\/h3\u003E\n\u003Col\u003E\n\t\u003Cli\u003ETijdens het boeken\u00a0dien je de betaling te voldoen aan ActievandeDag.\u003C\/li\u003E\n\t\u003Cli\u003EDit is exclusief de toeristenbelasting, deze kosten dien je ter plaatse te voldoen.\u003C\/li\u003E\n\t\u003Cli\u003ENa de betaling ontvang je per e-mail een bevestiging van ActievandeDag.\u003C\/li\u003E\n\t\u003Cli\u003EMet de bevestiging van ActievandeDag kun je inchecken in het hotel.\u003C\/li\u003E\n\t\u003Cli\u003EHet hotel zal GEEN bevestiging meer sturen.\u003C\/li\u003E\n\u003C\/ol\u003E","dealType":3,"isExpired":0,"isStealth":0,"isProduct":0,"isPrivate":0,"showInCategoryOnly":0},{"id":"253852","thumbnail":"\u003Cimg class=\u0022responsive-image pure-img\u0022 data-src=\u0022http:\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Van-der-Valk-Uden-Veghel-Comfort-kamer-421x265.jpg\u0022 src=\u0022http:\/\/www.actievandedag.nl\/wp-content\/themes\/actievandedag.nl-v4\/library\/images\/placeholder.png\u0022 \/\u003E","thumbnailUrl":"\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Van-der-Valk-Uden-Veghel-Comfort-kamer.jpg","thumbnailMediumUrl":"\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Van-der-Valk-Uden-Veghel-Comfort-kamer-503x370.jpg","bookingUrl":"https:\/\/www.actievandedag.nl\/boeken\/prijsberekening\/253852","startTime":1466719200,"endTime":1468015200,"title":"Luxe 4* hotel in Brabant","category":"Hotels","categoryType":"1","affiliateCategoryType":6,"categories":["Er op uit","Hotels","Hotelsoverig","Kortingscode","Liefdes Acties","Luxe hotels","Romantiek","Van der Valk"],"categoriesSlugs":["er-op-uit","hotels","hotelsoverig","kortingscode","liefdes-acties","luxe-hotels","romantiek","van-der-valk"],"images":["\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Van-der-Valk-Uden-Veghel-Comfort-kamers.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-7.jpg","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2016\/06\/shutterstock_434809081.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2016\/06\/shutterstock_102077245.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-19.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-16.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-9.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/06\/shutterstock_145508758.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-1.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-8.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-12.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/06\/shutterstock_284805803.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-17.jpg","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2016\/06\/shutterstock_380224150.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-18.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-5.jpg"],"imagesMedium":["\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Van-der-Valk-Uden-Veghel-Comfort-kamers-503x370.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-7.jpg","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2016\/06\/shutterstock_434809081-503x370.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/06\/shutterstock_102077245-503x370.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-19.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-16.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-9.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2016\/06\/shutterstock_145508758-503x370.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-1.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-8.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-12.jpg","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2016\/06\/shutterstock_284805803-503x370.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-17.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2016\/06\/shutterstock_380224150-503x370.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-18.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/02\/Van-der-Valk-Uden-5.jpg"],"permalink":"24-06-2016-van-der-valk-uden-veghel-noord-brabant","cornerText":"40% korting","rating":4,"offeringList":["4* Van der Valk Uden-Veghel","\n3 dagen + ontbijt en fietshuur","\nZoover-beoordeling: een 9.1"],"dealOrders":81,"position":1,"status":0,"author":"4611","adminTitle":"24-06-2016 Van der Valk Uden-Veghel Noord-Brabant","accomodationTitle":"Van der Valk Uden-Veghel","originalPricePrefix":"","originalPrice":265,"startingPricePrefix":"","startingPrice":158,"startingPricePostfix":"p. verblijf","savePercentage":40,"dealConfigurationId":8726,"invoiceInfo":"U heeft een 3-daags verblijf in Van der Valk Hotel Uden-Veghel geboekt. Dit arrangement is inclusief 2 overnachtingen in Van der Valk Uden - Veghel, 2x uitgebreid ontbijtbuffet, 1 dag fietshuur, gebruik WiFi en gratis parkeren.","extraInvoiceInfo":"U heeft de betaling voldaan aan ActievandeDag. U ontvangt van Van der Valk Hotel Uden Veghel binnen 5  werkdagen na afloop van de actie een definitieve bevestiging per e-mail. Deze bevestiging van Van der Valk Hotel Uden Veghel dient u uit te printen en mee te nemen naar het hotel op de dag van aankomst. Inchecken is mogelijk vanaf 14.00.\n\nActievandeDag fungeert als tussenpersoon bij het door u geboekte arrangement. Het hotel is verantwoordelijk voor de uitvoering van het arrangement. Het is niet mogelijk om uw boeking te annuleren of wijzigen bij ActievandeDag. Voor vragen over uw boeking, het wijzigen van uw boeking en\/of mogelijkheden voor annuleren, kunt u contact opnemen met het hotel via telefoonnummer +31 (0)41 379 90 10. Houd er rekening mee dat de annuleringsvoorwaarden van het hotel gelden en dat er eventueel kosten verbonden kunnen zijn aan het wijzigen van uw boeking. Bij annulering of no show heeft het hotel het recht om het gehele verblijf te factureren.\n\nAdres:\nVan der Valk Hotel Uden Veghel\nRondweg 2 \n5406 NK Uden","bccEmailAddress":"info@udenveghel.valk.com","ticketDescription":"","ticketInstructions":"","ticketImage":"","ticketColorAccent":"","partnerId":409658,"partnerName":"Van der Valk Uden","siteLabel":1308,"country":"","city":"Uden","latitude":51.671356,"longitude":5.603633,"accomodationProperties":[""],"carePackage":[""],"offeringShortDescription":"Wat is er nou fijner dan heerlijk fietsend het mooie Brabantse landschap ontdekken? Doe het deze zomer en slaap\u00a0in Van der Valk Uden-Veghel****.","tab1Label":"","tab2Label":"","tab3Label":"","tab1Content":"\u003Ch3\u003ERelaxen in\u0026nbsp;Noord-Brabant\u003C\/h3\u003E\n\u003Cul\u003E\n\u003Cli\u003E3 dagen in het luxe Van der Valk Hotel Uden-Veghel****\u003C\/li\u003E\n\u003Cli\u003EIncl. ontbijtbuffet, gratis parkeren en \u003Cstrong\u003E1 dag fietshuur\u003C\/strong\u003E\u003C\/li\u003E\n\u003Cli\u003EVerken de prachtige\u0026nbsp;omgeving van Noord-Brabant\u003C\/li\u003E\n\u003Cli\u003EBezoek bruisende steden als Den Bosch en Nijmegen\u003C\/li\u003E\n\u003Cli\u003E\u003Cstrong\u003EHet 4* hotel\u0026nbsp;is op Zoover beoordeeld met een 9.1\u003C\/strong\u003E\u003C\/li\u003E\n\u003C\/ul\u003E\n\u003Ch3\u003EInclusief\u003C\/h3\u003E\n\u003Col\u003E\n\u003Cli\u003E2 overnachtingen\u003C\/li\u003E\n\u003Cli\u003E2x uitgebreid ontbijtbuffet\u003C\/li\u003E\n\u003Cli\u003E1 dag fietshuur\u003C\/li\u003E\n\u003Cli\u003EGratis parkeren\u003C\/li\u003E\n\u003Cli\u003EGebruik WiFi\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EVan der Valk Hotel Uden-Veghel\u003C\/h3\u003E\n\u003Cp\u003EVan der Valk Hotel Uden-Veghel is een sfeervol en eigentijds ingericht hotel. Dit luxe 4* hotel beschikt over vele faciliteiten om jouw bezoek zo aangenaam mogelijk te maken. Zo kun je genieten van een hapje en een drankje in de skybar met live muziek. Ook heeft het hotel een heerlijk restaurant waar de chef-koks er genoegen in scheppen jou culinair te verwennen.\u003C\/p\u003E\n\u003Cp\u003E\u003Cstrong\u003EBeste hotel van Nederland 2015\u003C\/strong\u003E\u003Cbr \/\u003E Op 20 januari mocht Van der Valk Hotel Uden-Veghel een prijs in ontvangst nemen. Volgens reserveringssite Hotels.nl was Van der Valk Hotel Uden-Veghel namelijk het \u0026ldquo;Beste hotel van Nederland 2015\u0026rdquo;. Honderden gasten hebben het hotel o.a. beoordeeld op gastvrijheid, prijs-kwaliteit, locatie en hygi\u0026euml;ne.\u003C\/p\u003E\n\u003Cp\u003E\u003Cstrong\u003EDe\u0026nbsp;kamer\u003C\/strong\u003E\u003Cbr \/\u003E Je verblijft in een sfeervol ingerichte tweepersoonskamers met twee eenpersoonsbedden, voorzien van een eigentijds interieur. De kamers beschikken over moderne faciliteiten en hebben een luxe afwerking. De groots opgezette badkamer is voorzien van een ruime walk-in regenshower\u0026nbsp;en beschikt over een apart toilet. Ook is de kamer voorzien van een royaal bureau.\u003C\/p\u003E","tab2Content":"\u003Ch3\u003EOmgeving\u003C\/h3\u003E\n\u003Cp\u003E\u003Cstrong\u003EDe Maashorst\u003C\/strong\u003E\u003Cbr \/\u003E De gemeente Uden ligt aan de rand van natuurgebied De Maashorst. Met ruim 4000 hectare is dit het grootste natuurgebied van Brabant. Daarnaast zijn er nog diverse andere natuurgebieden. Deze gebieden lenen zicht uitstekend voor fiets- en wandeltochten.\u003C\/p\u003E\n\u003Cp\u003E\u003Cstrong\u003EUden\u003C\/strong\u003E\u003Cbr \/\u003E Uden staat bekend als winkelstad,\u0026nbsp;vanwege het gevarieerde en complete aanbod aan winkels. Na het winkelen kun je lekker bijkomen op \u0026eacute;\u0026eacute;n van de veel terrasjes in de binnenstad.\u0026nbsp;Naast winkelen en een terrasje pikken zijn een museumbezoek of lekker uit eten gaan ook een echte aanraders.\u003C\/p\u003E\n\u003Cp\u003E\u003Cstrong\u003EVijfmolen fietsroute\u003C\/strong\u003E\u003Cbr \/\u003E De \u0027Vijf Molenfietsroute\u0027 voert jullie door ca. 30 kilometer mooi landschap. Je kunt de route beginnen bij De Pelikaan (Maaskantje\/Den Dungen\/Sint-Michielsgestel), bij Catharinamolen (Schijndel) of de Molenromp aan de St. Lambertusweg in Gemonde.\u003C\/p\u003E\n\u003Cp\u003E\u003Cstrong\u003ESeiMei Museum\u003C\/strong\u003E\u003Cbr \/\u003E Bent je een liefhebber van kunst en bezoek je graag een galerie?\u0026nbsp;Stichting Industrieel Erfgoed Meijerij (SIEMei) in Veghel onderhoudt en toont machines waar bedrijven in de Meijerij hun bestaan aan te danken hebben.\u003C\/p\u003E\n\u003Ch3\u003ELocatie\u003C\/h3\u003E\n\u003Cp\u003E[sgmap addr=\u0022Van der Valk Hotel Uden Veghel Rondweg 2 5406 NK Uden\u0022]\u003C\/p\u003E","tab3Content":"\u003Ch3\u003EVoorwaarden\u003C\/h3\u003E\n\u003Col\u003E\n\u003Cli\u003EDeze aanbieding is alleen te boeken voor de aangegeven data.\u003C\/li\u003E\n\u003Cli\u003EDeze actie is alleen te boeken \u003Cstrong\u003Evan vrijdag 24 juni\u0026nbsp;00:00 uur t\/m vrijdag 8 juli\u0026nbsp;23:59 uur.\u003C\/strong\u003E\u003C\/li\u003E\n\u003Cli\u003EMaximaal 2 personen per kamer\/boeking, geen uitzonderingen mogelijk.\u003C\/li\u003E\n\u003Cli\u003EIndien je met meerdere personen wilt gaan, dien\u0026nbsp;je\u0026nbsp;meerdere boekingen te maken.\u003C\/li\u003E\n\u003Cli\u003ELet op: per datum is slechts een beperkt aantal kamers beschikbaar. Voor informatie over de beschikbaarheid per datum kun\u0026nbsp;je\u0026nbsp;contact opnemen met onze klantenservice.\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EKortingen en toeslagen\u003C\/h3\u003E\n\u003Col\u003E\n\u003Cli\u003EOp deze actie is geen kinderkorting van toepassing.\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EVoorwaarden hotel\u003C\/h3\u003E\n\u003Col\u003E\n\u003Cli\u003EInchecken vanaf 14.00 uur en uitchecken tot 11.00 uur.\u003C\/li\u003E\n\u003Cli\u003EGratis parkeergelegenheid in de omgeving van het hotel.\u003C\/li\u003E\n\u003Cli\u003EHuisdieren zijn in dit hotel niet toegestaan.\u003C\/li\u003E\n\u003Cli\u003EHet hotel is geschikt voor mindervaliden.\u003C\/li\u003E\n\u003Cli\u003EMinimale leeftijd hoofdboeker: 18 jaar.\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EWijzigen en annuleren\u003C\/h3\u003E\n\u003Col\u003E\n\u003Cli\u003EActievandeDag fungeert als tussenpersoon bij het door jou geboekte arrangement. Voor vragen over je boeking kun je contact opnemen met het hotel.\u003C\/li\u003E\n\u003Cli\u003EWijzigen of annuleren is niet mogelijk via ActievandeDag en Van Der Valk Hotel Uden-Veghel.\u003C\/li\u003E\n\u003Cli\u003EIn geval van annulering of no-show is restitutie niet mogelijk.\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EBoekingsproces\u003C\/h3\u003E\n\u003Col\u003E\n\u003Cli\u003ETijdens\u0026nbsp;het boeken dien je de betaling te voldoen aan ActievandeDag.\u003C\/li\u003E\n\u003Cli\u003ENa de betaling ontvang\u0026nbsp;je\u0026nbsp;per e-mail een bevestiging van ActievandeDag.\u003C\/li\u003E\n\u003Cli\u003EBinnen 5 werkdagen ontvang\u0026nbsp;je\u0026nbsp;per e-mail een bevestiging van Van der Valk Hotel Uden-Veghel.\u003C\/li\u003E\n\u003Cli\u003EMet de bevestiging van Van der Valk Hotel Uden-Veghel kun\u0026nbsp;je\u0026nbsp;inchecken in het hotel.\u003C\/li\u003E\n\u003C\/ol\u003E","dealType":3,"isExpired":0,"isStealth":0,"isProduct":0,"isPrivate":0,"showInCategoryOnly":0},{"id":"254292","thumbnail":"\u003Cimg class=\u0022responsive-image pure-img\u0022 data-src=\u0022http:\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Phantasialand.-421x265.jpg\u0022 src=\u0022http:\/\/www.actievandedag.nl\/wp-content\/themes\/actievandedag.nl-v4\/library\/images\/placeholder.png\u0022 \/\u003E","thumbnailUrl":"\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Phantasialand..jpg","thumbnailMediumUrl":"\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Phantasialand.-503x370.jpg","bookingUrl":"https:\/\/www.actievandedag.nl\/boeken\/prijsberekening\/254292","startTime":1467064800,"endTime":1468965600,"title":"Phantasialand incl. hotel","category":"","categoryType":"1","affiliateCategoryType":6,"categories":["Er op uit","Hotels","Hotelsoverig","Kortingscode","Tickets","Weg met kinderen","Zomervakantie"],"categoriesSlugs":["er-op-uit","hotels","hotelsoverig","kortingscode","tickets","weg-met-kinderen","zomervakantie"],"images":["\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Phantasialand.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Chiapas.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-10-421x265.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-17-421x265.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-15-421x265.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/21-421x265.png","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2015\/07\/158442145-421x265.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Balladins2-421x265.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-8-421x265.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/hoofd1-e1380109080861-421x265.png","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2015\/07\/lb_mysterycastle_302-421x265.jpg","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2015\/07\/11-421x2651.png","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2015\/07\/lb_riverquest_022-421x265.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/lb_wellenflug_012-421x265.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-19-421x265.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2015\/07\/3-421x2651.png","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-16-421x265.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-9-421x265.jpg","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-3-421x265.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-5-421x265.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-6-421x265.jpg"],"imagesMedium":["\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Phantasialand-503x370.jpg","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2016\/06\/Chiapas-503x370.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-10-421x265.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-17-421x265.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-15-421x265.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/21-421x265.png","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2015\/07\/158442145-421x265.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Balladins2-421x265.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-8-421x265.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/hoofd1-e1380109080861-421x265.png","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2015\/07\/lb_mysterycastle_302-421x265.jpg","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2015\/07\/11-421x2651.png","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2015\/07\/lb_riverquest_022-421x265.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/lb_wellenflug_012-421x265.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-19-421x265.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2015\/07\/3-421x2651.png","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-16-421x265.jpg","\/\/assets1.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-9-421x265.jpg","\/\/assets3.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-3-421x265.jpg","\/\/assets4.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-5-421x265.jpg","\/\/assets2.actievandedag.nl\/wp-content\/uploads\/2015\/07\/Phantasialand-6-421x265.jpg"],"permalink":"28-06-2016-balladins-hotel-keulen-incl-attractiepark-phantasialand","cornerText":"40% korting","rating":3,"offeringList":["Verblijf voor 2 personen incl. ontbijt","\nToegang Attractiepark Phantasialand","\nNieuw themagebied met achtbanen"],"dealOrders":69,"position":4,"status":0,"author":"568636","adminTitle":"28-06-2016 Balladins Hotel Keulen incl. Attractiepark Phantasialand","accomodationTitle":"EHM Hotel Cologne Airport Troisdorf","originalPricePrefix":"","originalPrice":248,"startingPricePrefix":"","startingPrice":149.9,"startingPricePostfix":"p. verblijf","savePercentage":40,"dealConfigurationId":7006,"invoiceInfo":"U heeft een 2-daags verblijf in EHM Hotel Cologne Airport Troisdorf geboekt. Dit arrangement is inclusief 1 overnachting in een 2-persoonskamert, 1x uitgebreid ontbijtbuffet, 1x toegangsticket voor Phantasialand per volwassene, 1 flesje water op de kamer, gratis parkeren bij het hotel en gratis WiFi.\n\nLet op: voor kinderen is de toegang tot Phantasialand niet inbegrepen. U kunt kaarten kopen in het hotel of bij de kassa van Phantasialand. De toegangsprijs voor kinderen is \u20ac 29,- per kind (4 t\/m 11 jaar).","extraInvoiceInfo":"U heeft de betaling voldaan aan ActievandeDag. Deze bevestiging dient u uit te printen en mee te nemen naar het hotel op de dag van aankomst. U ontvangt geen bevestiging meer van het hotel. Inchecken is mogelijk vanaf 14:00 uur.\n\nActievandeDag fungeert als tussenpersoon bij het door u geboekte arrangement. Balladins Superior Hotel K\u00f6ln Airport is verantwoordelijk voor de uitvoering van het arrangement. Het is niet mogelijk om uw boeking te annuleren of wijzigen bij ActievandeDag. Voor vragen over uw boeking, het wijzigen van uw boeking en\/of mogelijkheden voor annuleren, kunt u contact opnemen met Cologne hotel Airport via telefoonnummer +49 2241 9979. Houd er rekening mee dat de annuleringsvoorwaarden van het hotel gelden en dat er eventueel kosten verbonden kunnen zijn aan het wijzigen van uw boeking. Bij annulering of no show heeft het hotel het recht om het gehele verblijf te factureren.\n\nAdres:\nEHM Hotel Cologne Airport Troisdorf\nLarstra\u00dfe 1 \n53844 Troisdorf\nDuitsland","bccEmailAddress":"koeln-airport@balladins-hotels.com","ticketDescription":"","ticketInstructions":"","ticketImage":"","ticketColorAccent":"","partnerId":132391,"partnerName":"Balladins Hotels","siteLabel":1308,"country":"","city":"Keulen","latitude":50.807144,"longitude":7.131471,"accomodationProperties":[""],"carePackage":[""],"offeringShortDescription":"De spanning stijgt: Phantasialand opent een gloednieuw themagebied met 2 spectaculaire achtbanen. Wees er als eerste bij en boek dit hotelverblijf incl. entree.","tab1Label":"","tab2Label":"","tab3Label":"","tab1Content":"\u003Ch3\u003EBeleef een dag vol avontuur in Phantasialand\u003C\/h3\u003E\n\u003Cul\u003E\n\t\u003Cli\u003E1 nacht in Hotel Cologne Airport\u00a0hotel, nabij Keulen en Bonn\u003C\/li\u003E\n\t\u003Cli\u003EIncl. ontbijt, flets water, parkeren bij het hotel en gratis WiFi\u003C\/li\u003E\n\t\u003Cli\u003EInclusief een dag vol spanning en sensatie in Phantasialand\u003C\/li\u003E\n\t\u003Cli\u003EDit jaar: nieuw themagebied, 2 achtbanen, 6 wereldrecords\u003C\/li\u003E\n\t\u003Cli\u003EOntdek\u00a0lanceerachtbaan Taron en boomerang coaster Raik\u003C\/li\u003E\n\u003C\/ul\u003E\n\u003Ch3\u003EInclusief\u003C\/h3\u003E\n\u003Col\u003E\n\t\u003Cli\u003E1 overnachting voor 2 personen in een 2-persoonskamer\u003C\/li\u003E\n\t\u003Cli\u003E1 toegangsticket voor Phantasialand\u003Cstrong\u003E per volwassene\u003C\/strong\u003E\u003C\/li\u003E\n\t\u003Cli\u003E1x uitgebreid ontbijtbuffet\u003C\/li\u003E\n\t\u003Cli\u003EGratis fles water op de kamer\u003C\/li\u003E\n\t\u003Cli\u003EGratis parkeren bij het hotel\u003C\/li\u003E\n\t\u003Cli\u003EGratis WiFi\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EOptioneel bij te boeken\u003C\/h3\u003E\n\u003Col\u003E\n\t\u003Cli\u003EKinderen t\/m 11 jaar verblijven gratis bij 2 volbetalende personen op de kamer\n\u003Cstrong\u003ELet op: dit is exclusief toegang tot Phantasialand:\u00a0\u003C\/strong\u003E\u20ac 29,- per kind 4 t\/m 11 jaar. Kinderen jonger dan 4 jaar hebben gratis toegang tot Phantasialand.\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EEHM Hotel Cologne Airport Troisdorf***\u003C\/h3\u003E\nDe ideale locatie, uitstekende service en de familiaire sfeer vormen de basis voor de tevredenheid van de gasten. Dit mooie hotel heeft directe toegang tot snelweg A59 Keulen-Bonn Airport en is gelegen op slechts enkele kilometers van het commerci\u00eble centrum van Troisdorf. Zowel in het restaurant als op het terras van het hotel geniet je van een uitstekende en vriendelijke service. De gevarieerde wijnkaart en de heerlijke gerechten, van snacks tot multi-gangen menu, zorgen voor een uitstekend aanbod.\u00a0Door de nabijheid van de\u00a0steden\u00a0Keulen en Bonn \u00a0is het Cologne hotel Airport Troisdorf erg populair.\n\nMet deze topaanbieding krijg\u00a0je\u00a0ook een toegangskaart voor Phantasialand. Een dag vol vertier, spanning en sensatie voor jong en oud. Ervaar de vele attracties en doe spannende ontdekkingen!\n\u003Ch3\u003EDe kamers\u003C\/h3\u003E\nDit 3-sterren hotel beschikt over 88 comfortabele kamers met queen-size bedden, telefoon, flatscreen-tv, pay-tv en een haardroger. Kussens en dekbedden zijn geschikt voor mensen met een allergie.\n\u003Ch3\u003EPhantasialand\u003C\/h3\u003E\nPhantasialand is een uniek attractiepark voor jong en oud en staat bekend als \u00e9\u00e9n van de populairste parken van Europa. Je\u00a0vindt hier meer dan 40 spannende attracties, spectaculaire live shows en 6 indrukwekkende themawerelden: Fantasy, Mystery, Deep in Africa, Berlin, China Town en Mexico. Er zijn onder meer vier achtbanen, drie waterattracties en vier darkrides. Daarnaast zijn er leuke winkels, gezellige terrassen en fijne restaurants.\u00a0 Het ideale familie-uitje vol actie, avontuur, spanning en entertainment net over de grens.\n\nDe nieuwste attractie is Chiapas, een wildwaterbaan met maar liefst vijf verdiepingen! Een tochtje duurt zes minuten met onderweg drie afdalingen, waarvan \u00e9\u00e9n achterwaarts. Chiapas heeft niet alleen de grootste afdaling, maar ook \u2018s werelds steilste met een hellinghoek van 53 graden. Ontdek ook de andere superattracties zoals Black Mamba, Talocan, Colorado Adventure, Maus au Chocolat, River Quest, Winja\u2019s Fear \u0026amp; Force en Mystery Castle!","tab2Content":"\u003Ch3\u003EOmgeving\u003C\/h3\u003E\nHet Cologne Hotel Airport Troisdorf\u00a0is gelegen op kleine afstand van vliegveld Keulen-Bonn. Vanuit het hotel zijn deze twee steden ook goed te bereiken. Bezoek het mooie centrum van het bruisende Keulen, met de bekende Dom als middelpunt of ga naar Bonn, de voormalige hoofdstad van West-Duitsland. Beide steden hebben genoeg te bieden om een bezoekje de moeite waard te laten maken. Zowel op het gebied van cultuur als ontspanning ben je in deze twee steden aan het goede adres.\n\u003Ch3\u003EPhantasialand\u003C\/h3\u003E\nMet de auto ben je vanaf het hotel in\u00a0ongeveer een half uur bij Phantasialand. Je\u00a0beweegt je\u00a0van de ene wereld in de andere! Met meer dan 100 attracties en shows, verdeeld over 6 verschillende themagebieden verveel\u00a0je je\u00a0geen moment! Maak een ijzingwekkende rit door de Colorado Mountains met een ritje in de Colorado Adventure-achtbaan of maak een spetterende rit in de River Quest-wild-waterbaan.\n\nDurf\u00a0je\u00a0het aan om plaats te nemen in de Chiapas? Deze wildwaterbaan heeft de steilste afvaart ter wereld en gedurende 6 minuten krijg\u00a0je\u00a0gegarandeerd kippenvel door de kolkende waterstromen, duizenden exotische planten en indrukwekkende rotspartijen.\n\nBen je meer van de jungle en het spannende Afrika? In het themagedeelte dat \u0022Deep in Africa\u0022 heet, tref je de spannende achtbaan de \u0022Black Mamba\u0022 aan. Vernoemd naar een gevaarlijke slang doet deze achtbaan zijn naam eer aan terwijl hij met duizelingwekkende snelheid door diepe spleten en dwars door een waterval kronkelt!\n\nVerken daarnaast ook de wondere wereld van de Maya\u0027s of ontmoet het verloren gewaande volk de \u0027Wuzen\u0027. Na al dit avontuur kun je tot ontspanning komen in het Berlijn van 1900, een ideale plek om heerlijk tot rust te komen.\n\nNaast deze spannende achtbanen en attracties is er ook een grote selectie van gevarieerde en internationaal bekroonde musicals en entertainmentproducties. Leuk voor iedereen!\n\u003Ch3\u003ELocatie\u003C\/h3\u003E\n[sgmap addr=\u0022EHM Hotel cologne Airport Troisdorf\u00a0\u00a0Larstra\u00dfe 1 53844 Troisdorf Duitsland\u0022]","tab3Content":"\u003Ch3\u003EVoorwaarden actie\u003C\/h3\u003E\n\u003Col\u003E\n\t\u003Cli\u003EDeze actie is alleen te boeken voor de aangegeven data.\u003C\/li\u003E\n\t\u003Cli\u003EDeze actie is alleen te boeken \u003Cstrong\u003Evan dinsdag 28 juni\u00a000:00 uur t\/m dinsdag 19 juli\u00a023:59 uur.\u003C\/strong\u003E\u003C\/li\u003E\n\t\u003Cli\u003EMaximaal 2 volwassenen en 2 kinderen t\/m 11 jaar per boeking, geen uitzonderingen mogelijk\u003C\/li\u003E\n\t\u003Cli\u003EIndien je voor meerdere personen wil boeken, dien\u00a0je\u00a0meerdere boekingen te maken.\u003C\/li\u003E\n\t\u003Cli\u003ELet op: per datum is slechts een beperkt aantal kamers beschikbaar. Indien je\u00a0meerdere boekingen voor 1 datum wil maken, kun je voor informatie over de beschikbaarheid per datum contact opnemen met onze klantenservice.\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EKortingen en toeslagen\u003C\/h3\u003E\n\u003Col\u003E\n\t\u003Cli\u003E2 kinderen t\/m 11 jaar verblijven gratis bij 2 volbetalende personen op de kamer.\u00a0\u003Cstrong\u003EDit is exclusief toegangskaarten tot Phantasialand voor kinderen van 4 t\/m 11 jaar. Kinderen jonger dan 4 jaar hebben gratis toegang tot Phantasialand.\u003C\/strong\u003E\u003C\/li\u003E\n\t\u003Cli\u003EDe kaarten voor de kinderen kun je kopen in het hotel of bij de kassa van Phantasialand. De toegangsprijs voor kinderen is \u20ac 29,- per kind (4 t\/m 11 jaar).\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EVoorwaarden\u00a0hotel\u003C\/h3\u003E\n\u003Col\u003E\n\t\u003Cli\u003EInchecken vanaf 14:00 uur en uitchecken tot 12:00 uur.\u003C\/li\u003E\n\t\u003Cli\u003EKleine huisdieren zijn toegestaan: \u20ac 10,- per dier (max. 1, ter plaatse voldoen).\u003C\/li\u003E\n\t\u003Cli\u003EDe tickets voor Phantasialand ontvang je na het inchecken.\u003C\/li\u003E\n\t\u003Cli\u003EJe\u00a0kunt gratis parkeren bij het hotel\u003C\/li\u003E\n\t\u003Cli\u003EHet hotel is geschikt voor mindervaliden.\u003C\/li\u003E\n\t\u003Cli\u003EMinimale leeftijd hoofdboeker: 18 jaar.\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EWijzigen en annuleren\u003C\/h3\u003E\n\u003Col\u003E\n\t\u003Cli\u003EActievandeDag fungeert als tussenpersoon bij het door jou geboekte arrangement. Voor vragen over je boeking kun je contact opnemen met het hotel.\u003C\/li\u003E\n\t\u003Cli\u003EWijzigen of annuleren is niet mogelijk via ActievandeDag.\u003C\/li\u003E\n\t\u003Cli\u003EIn geval van annulering of no-show is restitutie niet mogelijk.\u003C\/li\u003E\n\u003C\/ol\u003E\n\u003Ch3\u003EBoekingsproces\u003C\/h3\u003E\n\u003Col\u003E\n\t\u003Cli\u003ETijdens het boeken\u00a0dien\u00a0je de betaling te voldoen aan ActievandeDag.\u003C\/li\u003E\n\t\u003Cli\u003ENa de betaling ontvang je\u00a0per e-mail een bevestiging van ActievandeDag.\u003C\/li\u003E\n\t\u003Cli\u003EMet de bevestiging van ActievandeDag kun\u00a0je\u00a0inchecken bij het hotel.\u003C\/li\u003E\n\t\u003Cli\u003EHet hotel zal je\u00a0GEEN bevestiging meer sturen.\u003C\/li\u003E\n\u003C\/ol\u003E","dealType":3,"isExpired":0,"isStealth":0,"isProduct":0,"isPrivate":0,"showInCategoryOnly":0}]}';
        $data = json_decode($json, true);

        return $data;
    }
}
